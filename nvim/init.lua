--vim.opt.packpath = "~/.local/share/nvim/site"

local fn = vim.fn
local install_path = require('packer').config.complie_path
--if fn.empty(fn.glob(install_path)) > 0 then
  --packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    -- vim.cmd "PackerSync"
--end
vim.cmd [[packadd packer.nvim]]
require "plugins"
require "impatient"
require "keymap"
require "options"
