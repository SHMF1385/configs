local M = {}

M.onedark = function()
	local onedark_ok, onedark = pcall(require, "onedark")

	if not onedark_ok then
		return
	end
	onedark.setup {
	    style = 'deep'
	}
	onedark.load()
end
M.mason = function()
	local mason_ok, mason = pcall(require, "mason")

	if not mason_ok then
		return
	end

	mason.setup()
end
M.nvchad_ui = function()
	local nvchad_ui_ok, nvchad_ui = pcall(require, "nvchad_ui")

	if not nvchad_ui_ok then
		return
	end

	nvchad_ui.setup()
end
M.gitsigns = function()
	local gitsigns_ok, gitsigns = pcall(require, "gitsigns")

	if not gitsigns_ok then
		return
	end

	require('gitsigns').setup {
	signs = {
	  add          = {hl = 'GitSignsAdd'   , text = '│', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
	  change       = {hl = 'GitSignsChange', text = '│', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
	  delete       = {hl = 'GitSignsDelete', text = '_', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
	  topdelete    = {hl = 'GitSignsDelete', text = '‾', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
	  changedelete = {hl = 'GitSignsChange', text = '~', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
	},
	signcolumn = true,  -- Toggle with `:Gitsigns toggle_signs`
	numhl      = false, -- Toggle with `:Gitsigns toggle_numhl`
	linehl     = false, -- Toggle with `:Gitsigns toggle_linehl`
	word_diff  = false, -- Toggle with `:Gitsigns toggle_word_diff`
	watch_gitdir = {
	  interval = 1000,
	  follow_files = true
	},
	attach_to_untracked = true,
	current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
	current_line_blame_opts = {
	  virt_text = true,
	  virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
	  delay = 1000,
	  ignore_whitespace = false,
	},
	current_line_blame_formatter = '<author>, <author_time:%Y-%m-%d> - <summary>',
	sign_priority = 6,
	update_debounce = 100,
	status_formatter = nil, -- Use default
	max_file_length = 40000, -- Disable if file is longer than this (in lines)
	preview_config = {
	-- Options passed to nvim_open_win
	border = 'single',
	style = 'minimal',
	relative = 'cursor',
	  row = 0,
	  col = 1
  	},
	yadm = {
	enable = false
	},
	}
end
M.bufferline = function()
	local bufferline_ok, bufferline = pcall(require, "bufferline")

	if not bufferline_ok then
		return
	end

	bufferline.setup{
		diagnostics = 'nvim_lsp',
		diagnostics_update_in_insert = false,
		separator_style = "thick"
	}
end
M.feline = function()
	local feline_ok, feline = pcall(require, "feline")
	if not feline_ok then
		return
	end
	feline.setup()
end
return M
