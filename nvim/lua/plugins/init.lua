vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'

	use {
		'neovim/nvim-lspconfig',
		config = function() require("plugins.configs.lspconfig") end
	}
	use {
		'hrsh7th/cmp-nvim-lsp',
		config = function() require("plugins.configs.cmp" ) end
	}
	use {
		'hrsh7th/cmp-buffer',
		'hrsh7th/cmp-path',
		'hrsh7th/cmp-cmdline',
		'hrsh7th/nvim-cmp'
	}
	use 'nvim-lua/plenary.nvim'
	use 'lewis6991/impatient.nvim'
	use {
		'akinsho/bufferline.nvim',
		config = function() require("plugins.configs.others").bufferline() end
	}
	use {
		'nvim-treesitter/nvim-treesitter',
		config = function() require("plugins.configs.treesitter") end
	}
	use {
		'windwp/nvim-autopairs',
		config = function() require("nvim-autopairs").setup() end
	}
	use {
		'lewis6991/gitsigns.nvim',
		config = function() require("plugins.configs.others").gitsigns() end
	}
	use 'kdheepak/lazygit.nvim'
	use {
		'numToStr/Comment.nvim',
		config = function() require("Comment").setup() end
	}
	use {
		'kyazdani42/nvim-tree.lua',
		requires = {
			'kyazdani42/nvim-web-devicons',
		},
		config = function() require ("plugins.configs.nvimtree") end
	}
	use {
		"williamboman/mason.nvim",
		config = function() require("plugins.configs.others").mason() end
	}
	use 'kyazdani42/nvim-web-devicons'
	use {
		'navarasu/onedark.nvim',
		config = function() require("plugins.configs.others").onedark() end
	}
	use {
		'NvChad/ui',
		config = function() require("plugins.configs.others").nvchad_ui() end
	}
	use {
	  "NvChad/nvterm",
	  config = function() require("plugins.configs.nvterm") end
	}
	use {
		'feline-nvim/feline.nvim',
		config = function() require("plugins.configs.others").feline() end
	}
	use {
			'editorconfig/editorconfig-vim',
			cpnfig = function() require('editorconfig-vim').setup() end
	}
	use {
			'nvim-telescope/telescope.nvim'
	}
end)
