-- Lazygit plugin configs
vim.g.lazygit_floating_window_winblend = 0
vim.g.lazygit_floating_window_scaling_factor = 0.9
vim.g.lazygit_floating_window_corner_chars = {'╭', '╮', '╰', '╯'}
vim.g.lazygit_floating_window_use_plenary = 1
vim.g.lazygit_use_neovim_remote = 1

vim.wo.wrap = false

local options = {
	number = true,
	termbidi = true,
	mouse = "a",
	termguicolors = true,
	splitbelow = true,
	splitright = true,
	wrap = true,
	tabstop = 4
}

for key, value in pairs(options) do
	vim.opt[key] = value
end
