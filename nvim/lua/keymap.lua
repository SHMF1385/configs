function map(mode, lhs, rhs, opts)
	local options = {noremap = true}
	if opts then options = vim.tbl_extend('force', options, opts) end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

map("n", "vs", ":vsplit")
map("n", "hs", ":hsplit")
map("n", "<space>e", "<cmd> :NvimTreeFocus<CR>")
map("n", "<space>g", "<cmd> :LazyGit<CR>") -- Launch lazygit within neovim
map("n", "<space>s", "<cmd> :set hlsearch!<CR>") -- Hide search highlights

-- Navigation and resize shortcuts for normal mode
map("n", "<C-Up>", "<C-w>j")
map("n", "<C-Down>", "<C-w>k")
map("n", "<C-Left>", "<C-w>h")
map("n", "<C-Right>", "<C-w>l")
map("n", "<C-S-Up>", "<cmd> :resize -2<CR>")
map("n", "<C-S-Down>", "<cmd> :resize +2<CR>")
map("n", "<C-S-Right>", "<cmd> :vertical resize +2<CR>")
map("n", "<C-S-Left>", "<cmd> :vertical resize -2<CR>")

-- Navigation and resize shortcuts for terminal mode
map("t", "<C-Up>", "<C-w>j")
map("t", "<C-Down>", "<C-w>k")
map("t", "<C-Left>", "<C-w>h")
map("t", "<C-Right>", "<C-w>l")
map("t", "<C-S-Up>", "<cmd> :resize +2<CR>")
map("t", "<C-S-Down>", "<cmd> :resize -2<CR>")
map("t", "<C-S-Right>", "<cmd> :vertical resize -2<CR>")
map("t", "<C-S-Left>", "<cmd> :vertical resize +2<CR>")

-- Telescope.nvim shortcuts
map("n", "<space>ff", "<cmd>Telescope find_files<CR>")
map("n", "<space>fg", "<cmd>Telescope live_grep<CR>")
